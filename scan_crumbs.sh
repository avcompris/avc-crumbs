#!/bin/sh
#
#	Usage:
#
#	$ ./scan_crumbs.sh [options] <path-to-folder>
#
#	Options:
#
#	-h print the help message
#	-l <path-to-logFile> — default: /tmp/avc-crumbs.log
#	-m <number-of-threads> — default: 1
#	-a also scan hidden files? — default: false
#	-z scan inside zip, gz, tar and tgz files? — default: false

set -e

CLASSPATH=

M2_REPO=~/.m2/repository
CLASSPATH=${CLASSPATH}:target/classes
CLASSPATH=${CLASSPATH}:${M2_REPO}/com/beust/jcommander/1.72/jcommander-1.72.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/com/google/code/findbugs/jsr305/3.0.0/jsr305-3.0.0.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/com/google/code/findbugs/jsr305/3.0.0/jsr305-3.0.0.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/com/google/guava/guava/23.0/guava-23.0.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/org/apache/commons/commons-lang3/3.4/commons-lang3-3.4.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/org/apache/commons/commons-collections4/4.0/commons-collections4-4.0.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/joda-time/joda-time/2.9.9/joda-time-2.9.9.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/commons-io/commons-io/2.4/commons-io-2.4.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/commons-codec/commons-codec/1.11/commons-codec-1.11.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/net/avcompris/commons/avc-commons3-databeans/0.0.5/avc-commons3-databeans-0.0.5.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/net/avcompris/commons/avc-commons3-types/0.0.5/avc-commons3-types-0.0.5.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/net/avcompris/commons/avc-commons3-yaml/0.0.5/avc-commons3-yaml-0.0.5.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/org/yaml/snakeyaml/1.24/snakeyaml-1.24.jar

CLASSPATH=${CLASSPATH}:avc-crumbs-0.0.1-SNAPSHOT-jar-with-dependencies.jar
CLASSPATH=${CLASSPATH}:target/avc-crumbs-0.0.1-SNAPSHOT-jar-with-dependencies.jar

java -cp "${CLASSPATH}" net.avcompris.crumbs.command.Main $*