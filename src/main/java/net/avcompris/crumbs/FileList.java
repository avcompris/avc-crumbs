package net.avcompris.crumbs;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.substringAfter;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.FileUtils;

public class FileList {

	public static FileList load(final File fileListFile, final String basedir) throws IOException {

		checkNotNull(fileListFile, "fileListFile");
		checkNotNull(basedir, "basedir");

		System.out.println("Loading: " + basedir + "...");

		final Set<String> paths = newHashSet();

		for (final String line : FileUtils.readLines(fileListFile, UTF_8)) {

			if (!line.startsWith(basedir)) {

				throw new IllegalStateException("basedir: " + basedir + ", line: " + line);
			}

			String path = substringAfter(line, basedir);

			while (path.length() > 0 && path.charAt(0) == '/') {

				path = path.substring(1);
			}

			// System.out.println(path);

			paths.add(path);
		}

		return new FileList(basedir, paths);
	}

	private final String basedir;
	private final Set<String> paths;

	private FileList(final String basedir, final Set<String> paths) {

		this.basedir = checkNotNull(basedir, "basedir");
		this.paths = checkNotNull(paths, "paths");
	}

	public String getBasedir() {

		return basedir;
	}

	public Iterable<String> getPaths() {

		return paths;
	}

	public boolean hasPath(final String path) {

		checkNotNull(path, "path");

		return paths.contains(path);
	}
}
