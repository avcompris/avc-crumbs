package net.avcompris.crumbs;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;
import static org.apache.commons.lang3.StringUtils.substringBetween;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

public class FileListCrumbs {

	private static Set<String> pathsInError = newHashSet();

	public static void addFilesInError(final FileList fileList) throws IOException {

		checkNotNull(fileList, "fileList");

		final String basedirAsString = fileList.getBasedir();

		System.out.println("basedir: " + basedirAsString);

		final File basedir = new File(basedirAsString);

		if (!basedir.isDirectory()) {

			throw new FileNotFoundException("basedir: " + basedirAsString);
		}

		for (final String path : fileList.getPaths()) {

			final String fullPath = basedirAsString + "/" + path;

			pathsInError.add(fullPath);
		}
	}

	public static void produceCrumbs(final FileList fileList, final File outFile) throws IOException {

		checkNotNull(fileList, "fileList");
		checkNotNull(outFile, "outFile");

		final String basedirAsString = fileList.getBasedir();

		System.out.println("basedir: " + basedirAsString);

		final File basedir = new File(basedirAsString);

		if (!basedir.isDirectory()) {

			throw new FileNotFoundException("basedir: " + basedirAsString);
		}

		final Set<String> alreadyLoadedPaths = newHashSet();

		if (outFile.isFile()) {

			for (final String line : FileUtils.readLines(outFile, UTF_8)) {

				@SuppressWarnings("unused")
				final String sha256 = substringBefore(line, " ");

				final String lengthAsString = substringBetween(line, " ");

				final String path = substringAfter(line, " " + lengthAsString + " ");

				alreadyLoadedPaths.add(path);

				System.out.println("alreadyLoadedPath: " + path);
			}
		}

		int count = 0;

		for (final String path : fileList.getPaths()) {

			++count;

			System.out.println(count + ": " + path);

			final String fullPath = basedirAsString + "/" + path;

			if (alreadyLoadedPaths.contains(fullPath)) {

				System.out.println("    (Skipping: Already loaded)");

				continue;

			} else if (pathsInError.contains(fullPath)) {

				System.out.println("    (Skipping: Marked as in error ***)");

				continue;
			}

			final File file = new File(basedir, path);

			if (!file.isFile()) {

				throw new FileNotFoundException("basedir: " + basedirAsString + ", path: " + path);
			}

			final long length = file.length();

			final String sha256;

			try (InputStream is = new FileInputStream(file)) {

				sha256 = DigestUtils.sha256Hex(is);
			}

			final String message = sha256 + " " + length + " " + fullPath;

			final boolean APPEND = true;

			FileUtils.write(outFile, message + "\n", UTF_8, APPEND);
		}
	}

	public static FileListCrumbs load(final File fileListFile, final String basedir) throws IOException {

		checkNotNull(fileListFile, "fileListFile");
		checkNotNull(basedir, "basedir");

		System.out.println("Loading: " + basedir + "...");

		final Map<String, FileCrumb> crumbs = newHashMap();

		for (final String line : FileUtils.readLines(fileListFile, UTF_8)) {

			final String sha256 = substringBefore(line, " ");

			final String lengthAsString = substringBetween(line, " ");

			String path = substringAfter(line, " " + lengthAsString + " ");

			if (!path.startsWith(basedir)) {

				throw new IllegalStateException("basedir: " + basedir + ", path: " + path);
			}

			path = substringAfter(line, basedir);

			while (path.length() > 0 && path.charAt(0) == '/') {

				path = path.substring(1);
			}

			// System.out.println(path);

			final long length = Long.parseLong(lengthAsString);

			final FileCrumb crumb = new FileCrumb(path, length, sha256);

			crumbs.put(path, crumb);
		}

		return new FileListCrumbs(basedir, crumbs);
	}

	private final String basedir;
	private final Map<String, FileCrumb> crumbs;

	private FileListCrumbs(final String basedir, final Map<String, FileCrumb> crumbs) {

		this.basedir = checkNotNull(basedir, "basedir");
		this.crumbs = checkNotNull(crumbs, "crumbs");
	}

	public String getBasedir() {

		return basedir;
	}

	public Iterable<String> getPaths() {

		return crumbs.keySet();
	}

	public boolean hasPath(final String path) {

		checkNotNull(path, "path");

		return crumbs.containsKey(path);
	}

	public FileCrumb getCrumb(final String path) {

		checkNotNull(path, "path");

		final FileCrumb crumb = crumbs.get(path);

		if (crumb == null) {

			throw new IllegalArgumentException("path: " + path);
		}

		return crumb;
	}
}
