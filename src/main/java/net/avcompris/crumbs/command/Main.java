package net.avcompris.crumbs.command;

import static net.avcompris.commons3.databeans.DataBeans.instantiate;

import java.io.PrintStream;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import net.avcompris.crumbs.CrumbScanner;
import net.avcompris.crumbs.ScanConfig;
import net.avcompris.crumbs.handler.ConsoleCrumbHandler;
import net.avcompris.crumbs.handler.CrumbHandler;

public class Main {

	public static void main(final String... args) {

		final int status = mainThatReturnsStatus(args);

		System.exit(status);
	}

	private static int mainThatReturnsStatus(final String... args) {

		final PrintStream out = System.out;
		final PrintStream err = System.err;

		final Command command = new Command();

		final JCommander jc = new JCommander(command);

		// jc.setProgramName("java -jar cmd-node.jar");

		try {

			jc.parse(args);

		} catch (final ParameterException e) {

			err.println(e);

			jc.usage();

			return -1;
		}

		if (command.isHelp()) {

			jc.usage();

			return 0;
		}

		final String srcPath = command.getSrcPath();

		final String logFile = "/tmp/avc-crumbs.log";

		final int numberOfThreads = command.getNumberOfThreads();

		final boolean scanHiddenFiles = command.getScanHiddenFiles();

		final boolean scanInsideArchiveFiles = command.getScanInsideArchiveFiles();

		final boolean dryRun = command.getDryRun();

		final ScanConfig config = instantiate(ScanConfig.class) //
				.setLogFile(logFile) //
				.setThreadCount(numberOfThreads) //
				.setScanHiddenFiles(scanHiddenFiles) //
				.setScanInsideArchiveFiles(scanInsideArchiveFiles) //
				.setDryRun(dryRun);

		final CrumbHandler handler;

		try {

			handler = new ConsoleCrumbHandler(out);

		} catch (final Throwable e) {

			e.printStackTrace(err);

			return -1;
		}

		final CrumbScanner scanner = new CrumbScanner( //
				out, //
				err, //
				handler);

		try {

			scanner.scan(srcPath, config);

		} catch (final Throwable e) {

			e.printStackTrace(err);

			return -1;
		}

		out.println("# Done.");

		return 0;
	}
}
