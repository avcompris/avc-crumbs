package net.avcompris.crumbs.command;

import com.beust.jcommander.Parameter;

class Command {

	@Parameter(description = "Source directory")
	private String srcPath = "/var/archives";

	public final String getSrcPath() {

		return srcPath;
	}

	@Parameter(description = "The path to the log file", names = { "-l", "--logfile" })
	private String pathToLogFile = "/tmp/avc-crumbs.log";

	public final String getPathToLogFile() {

		return pathToLogFile;
	}

	@Parameter(names = { "-h", "--help" }, help = true, description = "Display help information", arity = 0)
	private boolean help;

	public boolean isHelp() {

		return help;
	}

	@Parameter(description = "The number of threads", names = { "-m", "--multithread" })
	private int numberOfThreads = 1;

	public final int getNumberOfThreads() {

		return numberOfThreads;
	}

	@Parameter(description = "Do we skip SHA-256 calculation?", names = { "-n", "--dryrun" })
	private boolean dryRun = false;

	public final boolean getDryRun() {

		return dryRun;
	}

	@Parameter(description = "Do we scan hidden files?", names = { "-a", "--all" })
	private boolean scanHiddenFiles = false;

	public final boolean getScanHiddenFiles() {

		return scanHiddenFiles;
	}

	@Parameter(description = "Do we scan inside GZ, ZIP, TAR, TGZ files?", names = { "-z", "--zipped" })
	private boolean scanInsideArchiveFiles = false;

	public final boolean getScanInsideArchiveFiles() {

		return scanInsideArchiveFiles;
	}
}
