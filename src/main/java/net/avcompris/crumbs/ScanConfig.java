package net.avcompris.crumbs;

public interface ScanConfig {

	String getLogFile();

	ScanConfig setLogFile(String logFile);

	int getThreadCount();

	ScanConfig setThreadCount(int threadCount);

	boolean hasScanHiddenFiles();

	ScanConfig setScanHiddenFiles(boolean scanHiddenFiles);

	boolean hasScanInsideArchiveFiles();

	ScanConfig setScanInsideArchiveFiles(boolean scanInsideArchiveFiles);

	boolean hasDryRun();

	ScanConfig setDryRun(boolean dryRun);
}
