package net.avcompris.crumbs;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

import javax.annotation.Nullable;

public final class FileCrumb {

	public final String path;
	public final long length;
	public final String sha256;

	public FileCrumb(final String path, final long length, final String sha256) {

		this.path = checkNotNull(path, "path");
		this.length = length;
		this.sha256 = checkNotNull(sha256, "sha256");

		checkArgument(!isBlank(path), "path should not be blank: " + path);

		checkArgument(path.charAt(0) != '/', "path should not start with \"/\", but was: " + path);

		checkArgument(!isBlank(sha256), "sha256 should not be blank: " + sha256);

		checkArgument(sha256.length() == 64, "sha256.length should be 64, but was: " + sha256.length() + ": " + sha256);
	}

	@Override
	public int hashCode() {

		return Long.hashCode(length) + sha256.hashCode();
	}

	@Override
	public boolean equals(@Nullable final Object o) {

		if (o == null || !(o instanceof FileCrumb)) {

			return false;
		}

		final FileCrumb c = (FileCrumb) o;

		return path.equals(c.path) //
				&& length == c.length //
				&& sha256.equals(c.sha256);
	}
}
