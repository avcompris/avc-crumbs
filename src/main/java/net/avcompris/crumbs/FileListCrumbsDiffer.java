package net.avcompris.crumbs;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.substringAfterLast;

public class FileListCrumbsDiffer {

	public void listDiffs(final FileListCrumbs crumbs1, final FileListCrumbs crumbs2) {

		checkNotNull(crumbs1, "crumbs1");
		checkNotNull(crumbs2, "crumbs2");

		listDiffs_1_2(crumbs1, crumbs2);
		listDiffs_1_2(crumbs2, crumbs1);
	}

	private static void listDiffs_1_2(final FileListCrumbs crumbs1, final FileListCrumbs crumbs2) {

		System.out.println();

		System.out.println(crumbs1.getBasedir() + " --> " + crumbs2.getBasedir() + ":");

		int errorCount = 0;

		for (final String path : crumbs1.getPaths()) {

			if (!crumbs2.hasPath(path)) {

				System.out.println("  - " + path + " --> MISSING");

				++errorCount;

			} else {

				final FileCrumb crumb1 = crumbs1.getCrumb(path);
				final FileCrumb crumb2 = crumbs2.getCrumb(path);

				if (!crumb1.equals(crumb2)) {

					final String lastSlice = path.contains("/") //
							? substringAfterLast(path, "/") //
							: path;

					// Explicitly skip Mac OS X’s "/._xxx" files
					//
					if (!lastSlice.startsWith("._")) {

						System.out.println("  - " + path + ":");
						System.out.println("    " + crumb1.length + " --> " + crumb2.length);
						System.out.println("    " + crumb1.sha256 + " --> " + crumb2.sha256);

						++errorCount;
					}
				}
			}
		}

		if (errorCount == 0) {

			System.out.println("No missing file, all clear.");

		} else {

			System.out.println("ERRORS: " + errorCount);
		}

		System.out.println();
	}
}
