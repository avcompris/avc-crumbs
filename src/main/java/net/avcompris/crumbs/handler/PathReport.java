package net.avcompris.crumbs.handler;

interface PathReport {

	String getPath();

	String getName();

	PathReport setPath(String path);

	PathReport setName(String name);
}
