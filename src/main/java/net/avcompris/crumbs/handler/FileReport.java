package net.avcompris.crumbs.handler;

public interface FileReport extends PathReport {

	@Override
	FileReport setPath(String path);

	@Override
	FileReport setName(String name);

	long getLength();

	FileReport setLength(long length);

	String getSha256();

	FileReport setSha256(String sha256);
}
