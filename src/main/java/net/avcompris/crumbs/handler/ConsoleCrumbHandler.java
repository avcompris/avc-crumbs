package net.avcompris.crumbs.handler;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.crumbs.handler.EscapeUtils.escape;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import com.google.common.io.Resources;

import net.avcompris.crumbs.CrumbError;
import net.avcompris.crumbs.ScanConfig;

public final class ConsoleCrumbHandler implements CrumbHandler {

	private final PrintStream out;
	private final String scannerVersion;
	private boolean crumbsHaveStarted;

	public ConsoleCrumbHandler(final PrintStream out) throws IOException {

		this.out = checkNotNull(out, "out");

		final Properties properties = new Properties();

		properties.load( //
				new ByteArrayInputStream( //
						Resources.toByteArray( //
								Resources.getResource("appinfo.properties"))));

		scannerVersion = properties.getProperty("scannerVersion");

		if (isBlank(scannerVersion)) {
			throw new IllegalStateException("scannerVersion should have been set through Resource: appinfo.properties");
		}

		if (scannerVersion.contains("$")) {
			throw new IllegalStateException("scannerVersion should not contain \"$\", but was: " + scannerVersion);
		}
	}

	@Override
	public void startScan( //
			final DateTime startedAt, //
			final String hostname, //
			final String srcPath, //
			final CrumbType type, //
			final ScanConfig config //
	) throws IOException {

		checkNotNull(startedAt, "startedAt");
		checkNotNull(hostname, "hostname");
		checkNotNull(srcPath, "srcPath");
		checkNotNull(type, "type");
		checkNotNull(config, "config");

		out.println("version: " + scannerVersion);

		out.println("scan:");
		out.println("  startedAt: " + startedAt);
		out.println("  hostname: " + hostname);
		out.println("  srcPath: " + srcPath);
		out.println("  type: " + type);
		out.println("  config:");
		out.println("    logFile: " + config.getLogFile());
		out.println("    threadCount: " + config.getThreadCount());
		out.println("    scanHiddenFiles: " + config.hasScanHiddenFiles());
		out.println("    scanInsideArchiveFiles: " + config.hasScanInsideArchiveFiles());
	}

	@Override
	public void endScan( //
			final DateTime endedAt, //
			final long elapsedMs, //
			final int folderCount, //
			final int fileCount, //
			final long totalFileSize, //
			final int archiveFileCount, //
			final int errorCount //
	) throws IOException {

		checkNotNull(endedAt, "endedAt");

		out.println("status:");
		out.println("  endedAt: " + endedAt);
		out.println("  elapsedMs: " + elapsedMs);
		out.println("  folderCount: " + folderCount);
		out.println("  fileCount: " + fileCount);
		out.println("  totalFileSize: " + totalFileSize);
		out.println("  archiveFileCount: " + archiveFileCount);
		out.println("  errorCount: " + errorCount);
	}

	private void ensureCrumbs() throws IOException {

		if (!crumbsHaveStarted) {

			System.out.println("crumbs:");

			crumbsHaveStarted = true;
		}
	}

	@Override
	public void addFolder( //
			final String path, //
			final String fileName, //
			@Nullable final CrumbError error //
	) throws IOException {

		checkNotNull(path, "path");

		if (isBlank(path)) {

			// Skip root directory

			return;
		}

		synchronized (this) {

			ensureCrumbs();

			System.out.println("  - type: " + CrumbType.folder);
			System.out.println("    path: " + escape(path));
			System.out.println("    name: " + escape(fileName));
		}
	}

	@Override
	public void addFile( //
			final String path, //
			final String fileName, //
			final long length, //
			final DateTime lastModifiedAt, //
			@Nullable final String sha256, //
			@Nullable final CrumbError error //
	) throws IOException {

		checkNotNull(path, "path");
		checkNotNull(lastModifiedAt, "lastModifiedAt");

		synchronized (this) {

			ensureCrumbs();

			System.out.println("  - type: " + CrumbType.file);
			System.out.println("    path: " + escape(path));
			System.out.println("    name: " + escape(fileName));
			System.out.println("    length: " + length);
			System.out.println("    lastModifiedAt: " + lastModifiedAt);

			if (sha256 != null) {
				System.out.println("    sha256: " + sha256);
			}
		}
	}
}
