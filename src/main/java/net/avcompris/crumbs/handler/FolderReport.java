package net.avcompris.crumbs.handler;

public interface FolderReport extends PathReport {

	@Override
	FolderReport setPath(String path);

	@Override
	FolderReport setName(String name);
}
