package net.avcompris.crumbs.handler;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;

import net.avcompris.crumbs.CrumbError;
import net.avcompris.crumbs.ScanConfig;
import net.avcompris.crumbs.report.Config;
import net.avcompris.crumbs.report.ScanReport;

public final class ReportCrumbHandler implements CrumbHandler, ScanReport {

	private int folderCount = -1;
	private int fileCount = -1;
	private long totalFileSize = -1L;
	private int archiveFileCount = -1;
	private int errorCount = -1;

	private final Map<String, PathReport> crumbs = new ConcurrentHashMap<>();

	private final String hostname;
	private final String srcPath;

	public ReportCrumbHandler( //
			final String hostname, //
			final String srcPath //
	) {
		this.hostname = checkNotNull(hostname, "hostname");
		this.srcPath = checkNotNull(srcPath, "srcPath");
	}

	@Override
	public int getFolderCount() {

		return folderCount;
	}

	@Override
	public int getFileCount() {

		return fileCount;
	}

	@Override
	public long getTotalFileSize() {

		return totalFileSize;
	}

	@Override
	public int getArchiveFileCount() {

		return archiveFileCount;
	}

	@Override
	public int getErrorCount() {

		return errorCount;
	}

	@Override
	public FileReport getFileReport(final String path) {

		checkNotNull(path, "path");

		final PathReport crumb = crumbs.get(path);

		if (crumb == null) {
			throw new NullPointerException("No entry at path: " + path);
		}

		if (crumb instanceof FileReport) {
			return (FileReport) crumb;
		}

		throw new IllegalArgumentException(
				"Entry is not a File at path: " + path + ", class: " + crumb.getClass().getName());
	}

	@Override
	public FolderReport getFolderReport(final String path) {

		checkNotNull(path, "path");

		final PathReport crumb = crumbs.get(path);

		if (crumb == null) {
			throw new NullPointerException("No entry at path: " + path);
		}

		if (crumb instanceof FolderReport) {
			return (FolderReport) crumb;
		}

		throw new IllegalArgumentException(
				"Entry is not a Folder at path: " + path + ", class: " + crumb.getClass().getName());
	}

	@Override
	public void startScan( //
			final DateTime startedAt, //
			final String hostname, //
			final String srcPath, //
			final CrumbType type, //
			final ScanConfig config //
	) throws IOException {

		checkNotNull(startedAt, "startedAt");
		checkNotNull(hostname, "hostname");
		checkNotNull(srcPath, "srcPath");
		checkNotNull(type, "type");
		checkNotNull(config, "config");

		// do nothing
	}

	@Override
	public void endScan( //
			final DateTime endedAt, //
			final long elapsedMs, //
			final int folderCount, //
			final int fileCount, //
			final long totalFileSize, //
			final int archiveFileCount, //
			final int errorCount //
	) throws IOException {

		checkNotNull(endedAt, "endedAt");

		this.folderCount = folderCount;
		this.fileCount = fileCount;
		this.totalFileSize = totalFileSize;
		this.archiveFileCount = archiveFileCount;
		this.errorCount = errorCount;
	}

	@Override
	public void addFolder( //
			final String path, //
			final String fileName, // 
			@Nullable final CrumbError error //
	) throws IOException {

		checkNotNull(path, "path");

		final PathReport old = crumbs.putIfAbsent(path, instantiate(FolderReport.class) //
				.setPath(path) //
				.setName(fileName));

		checkState(old == null, "Duplicate entry at path: " + path);
	}

	@Override
	public void addFile( //
			final String path, //
			final String fileName, // 
			final long length, //
			final DateTime lastModifiedAt, //
			@Nullable final String sha256, //
			@Nullable final CrumbError error //
	) throws IOException {

		checkNotNull(path, "path");
		checkNotNull(lastModifiedAt, "lastModifiedAt");

		final PathReport old = crumbs.putIfAbsent(path, instantiate(FileReport.class) //
				.setPath(path) //
				.setName(fileName) //
				.setLength(length) //
				.setSha256(sha256));

		checkState(old == null, "Duplicate entry at path: " + path);
	}

	@Override
	public String getHostname() {

		return hostname;
	}

	@Override
	public String getSrcPath() {

		return srcPath;
	}

	@Override
	public CrumbType getType() {

		throw new NotImplementedException("");
	}

	@Override
	public Config getConfig() {

		throw new NotImplementedException("");
	}
}
