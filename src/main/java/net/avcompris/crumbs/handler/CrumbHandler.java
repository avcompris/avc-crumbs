package net.avcompris.crumbs.handler;

import java.io.IOException;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.crumbs.CrumbError;
import net.avcompris.crumbs.ScanConfig;

public interface CrumbHandler {

	void startScan( //
			DateTime startedAt, //
			String hostname, //
			String srcPath, //
			CrumbType type, //
			ScanConfig config //
	) throws IOException;

	void endScan( //
			DateTime endedAt, //
			long elapsedMs, //
			int folderCount, //
			int fileCount, //
			long totalFileSize, //
			int archiveFileCount, //
			int errorCount //
	) throws IOException;

	void addFolder( //
			String path, //
			String fileName, //
			@Nullable CrumbError error //
	) throws IOException;

	void addFile( //
			String path, //
			String fileName, //
			long length, //
			DateTime lastModifiedAt, //
			@Nullable String sha256, //
			@Nullable CrumbError error //
	) throws IOException;

}
