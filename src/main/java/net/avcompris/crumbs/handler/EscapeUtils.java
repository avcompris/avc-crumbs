package net.avcompris.crumbs.handler;

import org.apache.commons.lang3.NotImplementedException;

public abstract class EscapeUtils {

	public static String escape(final String s) {

		for (final char c : s.toCharArray()) {

			if (!accept(c)) {

				return escapeAllChars(s);
			}
		}

		return s;
	}

	private static boolean accept(final char c) {

		if (c >= '0' && c <= '9' //
				|| c >= 'a' && c <= 'z' //
				|| c >= 'A' && c <= 'Z') {

			return true;
		}

		switch (c) {

		// ESCAPED

		case '\\':
		case '"':

			return false;

		// ASCII

		case ' ': // 32
		case '-':
		case '+':
		case '/':
		case '_':
		case '(':
		case ')':
		case '[':
		case ']':

		case '<':
		case '>':
		case '&':
		case '.':
		case ',':
		case '?':
		case '!':
		case '°':
		case '#': // 35
		case '$': // 36
		case '%': // 37
		case ';': // 59
		case '=': // 61
		case '@': // 64
		case '`': // 96
		case '{':
		case '}': // 125
		case '~': // 126

			// ACCENTS

		case '¿':
		case '¡':
		case 'á':
		case 'â':
		case 'à':
		case 'ä': // 228
		case 'ã':
		case 'æ':
		case 'ç':
		case 'č':
		case 'é':
		case 'ê':
		case 'è':
		case 'ë':
		case 'í': // 237
		case 'î':
		case 'ì': // 236
		case 'ï':
		case 'ñ':
		case 'ó':
		case 'ô':
		case 'ò': // 242
		case 'ö':
		case 'ø': // 248
		case 'œ':
		case 'ř': // 345
		case 'ú': // 250
		case 'û': // 251
		case 'ù':
		case 'ü':
		case 'Á':
		case 'Â':
		case 'À':
		case 'Ä':
		case 'Ã':
		case 'Æ':
		case 'Ç':
		case 'Č': // 268
		case 'É':
		case 'Ê':
		case 'È':
		case 'Ë':
		case 'Í':
		case 'Î':
		case 'Ì':
		case 'Ï':
		case 'Ñ':
		case 'Ó':
		case 'Ô':
		case 'Ò': // 242
		case 'Ö':
		case 'Ø':
		case 'Œ':
		case 'Ř':
		case 'Ú':
		case 'Û':
		case 'Ù':
		case 'Ü':

			// OTHERS

		case '': // 128
		case '': // 129
		case '': // 131
		case ' ': // 160
		case '¨': // 168
		case '©': // 169
		case '®': // 174
		case '´': // 180
		case '·': // 183
		case 'º': // 186
		case 'Ļ': // 315
		case 'ļ': // 316
		case 'ƒ': // 402
		case 'Ʒ': // 439
		case 'Ǽ': // 508
		case 'ʥ': // 677
		case '˫': // 747
		case '̳': // 819
		case 'ϵ': // 1013
		case 'Ϲ': // 1017
		case 'Д': // 1044
		case 'Е': // 1045
		case 'З': // 1047
		case 'Ч': // 1063
		case 'Ь': // 1068
		case 'Я': // 1071
		case 'а': // 1072
		case 'г': // 1075
		case 'д': // 1076
		case 'з': // 1079
		case 'и': // 1080
		case 'й': // 1081
		case 'к': // 1082
		case 'п': // 1087
		case 'с': // 1089
		case 'ц': // 1094
		case 'щ': // 1097
		case 'ы': // 1099
		case 'ь': // 1100
		case 'һ': // 1211
		case 'Ӣ': // 1250
		case 'ӭ': // 1261
		case 'Ӱ': // 1264
		case '԰': // 1328
		case 'վ': // 1406
		case 'ا': // 1575
		case 'ب': // 1576
		case 'ة': // 1577
		case 'ر': // 1585
		case 'ع': // 1593
		case 'ل': // 1604
		case 'ي': // 1610
		case '‌': // 8204
		case '‐': // 8208
		case '–': // 8211
		case '—': // 8212
		case '’': // 8217
		case '“': // 8220
		case '”': // 8221
		case '…': // 8230
		case '™': // 8482
		case '⛦': // 9958
		case '⭐': // 11088
		case '《': // 12298
		case '》': // 12299
		case '上': // 19978
		case '下': // 19979
		case '中': // 20013
		case '事': // 20107
		case '享': // 20139
		case '人': // 20154
		case '以': // 20197
		case '件': // 20214
		case '众': // 20247
		case '会': // 20250
		case '作': // 20316
		case '供': // 20379
		case '克': // 20811
		case '全': // 20840
		case '公': // 20844
		case '关': // 20851
		case '分': // 20998
		case '到': // 21040
		case '制': // 21046
		case '剧': // 21095
		case '力': // 21147
		case '动': // 21160
		case '募': // 21215
		case '升': // 21319
		case '压': // 21387
		case '发': // 21457
		case '叔': // 21460
		case '取': // 21462
		case '号': // 21495
		case '合': // 21512
		case '同': // 21516
		case '后': // 21518
		case '多': // 22810
		case '夫': // 22827
		case '好': // 22909
		case '如': // 22914
		case '字': // 23383
		case '官': // 23448
		case '布': // 24067
		case '帝': // 24093
		case '幕': // 24149
		case '库': // 24211
		case '彗': // 24407
		case '影': // 24433
		case '快': // 24555
		case '您': // 24744
		case '感': // 24863
		case '或': // 25110
		case '扫': // 25195
		case '招': // 25307
		case '提': // 25552
		case '故': // 25925
		case '文': // 25991
		case '新': // 26032
		case '日': // 26085
		case '时': // 26102
		case '星': // 26143
		case '晨': // 26216
		case '更': // 26356
		case '曼': // 26364
		case '最': // 26368
		case '期': // 26399
		case '本': // 26412
		case '果': // 26524
		case '欢': // 27426
		case '此': // 27492
		case '比': // 27604
		case '注': // 27880
		case '流': // 27969
		case '清': // 28165
		case '源': // 28304
		case '版': // 29256
		case '特': // 29305
		case '由': // 30001
		case '的': // 30340
		case '看': // 30475
		case '码': // 30721
		case '磁': // 30913
		case '站': // 31449
		case '系': // 31995
		case '级': // 32423
		case '组': // 32452
		case '统': // 32479
		case '网': // 32593
		case '美': // 32654
		case '群': // 32676
		case '翻': // 32763
		case '联': // 32852
		case '自': // 33258
		case '英': // 33521
		case '获': // 33719
		case '视': // 35270
		case '译': // 35793
		case '语': // 35821
		case '请': // 35831
		case '谢': // 35874
		case '资': // 36164
		case '轴': // 36724
		case '载': // 36733
		case '迎': // 36814
		case '进': // 36827
		case '长': // 38271
		case '间': // 38388
		case '霍': // 38669
		case '韩': // 38889
		case '风': // 39118
		case '高': // 39640
		case '魔': // 39764
		case '鸟': // 40479
		case '鸣': // 40483
		case '麦': // 40614
		case '️': // 65039
		case '，': // 65292
		case '：': //65306
		case '�': // 65533

			return true;

		default:

			return false;
		}
	}

	private static String escapeAllChars(final String s) {

		final StringBuilder sb = new StringBuilder('"');

		for (final char c : s.toCharArray()) {

			if (accept(c)) {

				sb.append(c);

			} else if (c == '\\') {

				sb.append("\\\\");

			} else if (c == '\'') {

				sb.append(c);

			} else if (c == '"') {

				sb.append("\\\"");

			} else {

				throw new NotImplementedException("char: " + c + " (" + ((int) c) + "), in: " + s);
			}
		}

		return sb.append('"').toString();
	}
}
