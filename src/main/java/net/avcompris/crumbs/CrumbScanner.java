package net.avcompris.crumbs;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_256;
import static org.joda.time.DateTimeZone.UTC;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;

import net.avcompris.crumbs.handler.CrumbHandler;
import net.avcompris.crumbs.handler.CrumbType;

public class CrumbScanner {

	private final PrintStream out;
	private final PrintStream err;
	private final CrumbHandler handler;

	private int folderCount = 0;
	private int fileCount = 0;
	private long totalFileSize = 0L;
	private int archiveFileCount = 0;
	private int errorCount = 0;

	public CrumbScanner(final PrintStream out, final PrintStream err, final CrumbHandler handler) {

		this.out = checkNotNull(out, "out");
		this.err = checkNotNull(err, "err");
		this.handler = checkNotNull(handler, "handler");
	}

	public void scan(final String srcPath, final ScanConfig config) throws IOException {

		checkNotNull(srcPath, "srcPath");

		final File src = new File(srcPath);

		scan(src, config);
	}

	public void scan(final File src, final ScanConfig config) throws IOException {

		checkNotNull(src, "src");

		final String srcPath = src.getCanonicalPath();

		if (!src.exists()) {
			throw new FileNotFoundException("src: " + srcPath);
		}

		// out.println("Source: " + srcPath);

		final DateTime startedAt = new DateTime();

		final String hostname = "xxx";

		final Scanner scanner = new Scanner(config);

		if (src.isFile()) {

			handler.startScan(startedAt, hostname, srcPath, CrumbType.file, config);

			scanner.scanFile(src.getName(), src);

		} else if (src.isDirectory()) {

			handler.startScan(startedAt, hostname, srcPath, CrumbType.folder, config);

			scanner.scanFolder("", src);

		} else {

			throw new NotImplementedException("src should either be a File or a Directory");
		}

		final DateTime endedAt = new DateTime();

		final long elapsedMs = endedAt.getMillis() - startedAt.getMillis();

		handler.endScan(endedAt, elapsedMs, folderCount, fileCount, totalFileSize, archiveFileCount, errorCount);
	}

	private class Scanner {

		private final boolean dryRun;

		public Scanner(final ScanConfig config) {

			checkNotNull(config, "config");

			dryRun = config.hasDryRun();
		}

		public void scanFile(final String path, final File file) throws IOException {

			CrumbError error = null;

			final long length = file.length();

			final long lastModifiedAtMs = file.lastModified();

			final DateTime lastModifiedAt = new DateTime(lastModifiedAtMs).withZone(UTC);

			final String sha256 = dryRun //
					? null //
					: new DigestUtils(SHA_256).digestAsHex(file);

			final String fileName = file.getName();

			handler.addFile(path, fileName, length, lastModifiedAt, sha256, error);

			++fileCount;

			totalFileSize += length;
		}

		public void scanFolder(final String path, final File folder) throws IOException {

			CrumbError error = null;

			final String fileName = folder.getName();

			handler.addFolder(path, fileName, error);

			++folderCount;

			final List<File> files = newArrayList();

			for (final File file : folder.listFiles()) {

				files.add(file);
			}

			final String prefix = "".equals(path) ? "" : (path + "/");

			for (final File file : files) {

				if (file.isFile()) {

					scanFile(prefix + file.getName(), file);

				} else if (file.isDirectory()) {

					scanFolder(prefix + file.getName(), file);
				}
			}
		}
	}
}
