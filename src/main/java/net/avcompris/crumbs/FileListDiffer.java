package net.avcompris.crumbs;

import static com.google.common.base.Preconditions.checkNotNull;

public class FileListDiffer {

	public void listDiffs(final FileList fileList1, final FileList fileList2) {

		checkNotNull(fileList1, "fileList1");
		checkNotNull(fileList2, "fileList2");

		listDiffs_1_2(fileList1, fileList2);
		listDiffs_1_2(fileList2, fileList1);
	}

	private static void listDiffs_1_2(final FileList fileList1, final FileList fileList2) {

		System.out.println();

		System.out.println(fileList1.getBasedir() + " --> " + fileList2.getBasedir() + ":");

		int errorCount = 0;

		for (final String path : fileList1.getPaths()) {

			if (!fileList2.hasPath(path)) {

				System.out.println("  - " + path + " --> MISSING");

				++errorCount;
			}
		}

		if (errorCount == 0) {

			System.out.println("No missing file, all clear.");

		} else {

			System.out.println("ERRORS: " + errorCount);
		}
	}
}
