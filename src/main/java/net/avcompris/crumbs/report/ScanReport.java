package net.avcompris.crumbs.report;

import net.avcompris.crumbs.handler.CrumbType;
import net.avcompris.crumbs.handler.FileReport;
import net.avcompris.crumbs.handler.FolderReport;

public interface ScanReport {

	String getHostname();

	String getSrcPath();

	CrumbType getType();

	Config getConfig();

	int getFolderCount();

	int getFileCount();

	long getTotalFileSize();

	int getArchiveFileCount();

	int getErrorCount();

	FileReport getFileReport(String path);

	FolderReport getFolderReport(String path);
}
