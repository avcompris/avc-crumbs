package net.avcompris.crumbs.report;

public interface Config {

	String getLogFile();

	int getThreadCount();

	boolean getScanHiddenFiles();

	boolean getScanInsideArchiveFiles();
}
