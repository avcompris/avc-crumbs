package net.avcompris.crumbs.report;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang3.NotImplementedException;

import net.avcompris.commons3.yaml.Yaml;
import net.avcompris.commons3.yaml.YamlUtils;
import net.avcompris.crumbs.handler.CrumbType;
import net.avcompris.crumbs.handler.FileReport;
import net.avcompris.crumbs.handler.FolderReport;

public abstract class ScanReportUtils {

	public static ScanReport loadReport(final File yamlFile) throws IOException {

		final Yaml yaml = YamlUtils.loadYaml(yamlFile);

		return loadReport(yaml);
	}

	public static ScanReport loadReport(final Yaml yaml) {

		checkNotNull(yaml, "yaml");

		yaml.get("version").asString();

		final Yaml scanInfo = yaml.get("scan");

		final CrumbType type = CrumbType.valueOf(scanInfo.get("type").asString());

		// final int folderCount;
		// final int fileCount;
		// final long totalFileSize;
		// final int archiveFileCount;
		// final int errorCount;

		final Map<String, FolderReport> folderReports = newHashMap();
		final Map<String, FileReport> fileReports = newHashMap();

		if (type == CrumbType.folder) {

			folderReports.put("", instantiate(FolderReport.class) //
					.setPath(""));
		}

		if (yaml.has("crumbs")) {

			final Yaml crumbs = yaml.get("crumbs");

			for (final Yaml item : crumbs.items()) {

				final CrumbType itemType = CrumbType.valueOf(item.get("type").asString());
				final String path = item.get("path").asString();

				switch (itemType) {

				case folder:
					folderReports.put(path, instantiate(FolderReport.class) //
							.setPath(path));
					break;

				case file:
					final long length = item.get("length").asLong();
					final String sha256 = item.get("sha256").asString();
					fileReports.put(path, instantiate(FileReport.class) //
							.setPath(path) //
							.setLength(length) //
							.setSha256(sha256));
					break;

				default:
					throw new NotImplementedException("itemType: " + itemType);
				}
			}
		}

		final String hostname = scanInfo.get("hostname").asString();
		final String srcPath = scanInfo.get("srcPath").asString();

		final Yaml config = scanInfo.get("config");

		final String logFile = config.get("logFile").asString();
		final int threadCount = config.get("threadCount").asInt();
		final boolean scanHiddenFiles = config.get("scanHiddenFiles").asBoolean();
		final boolean scanInsideArchiveFiles = config.get("scanInsideArchiveFiles").asBoolean();

		return new ScanReport() {

			@Override
			public int getFolderCount() {

				return folderReports.size();
			}

			@Override
			public int getFileCount() {

				return fileReports.size();
			}

			@Override
			public long getTotalFileSize() {

				long totalFileSize = 0L;

				for (final FileReport fileReport : fileReports.values()) {

					totalFileSize += fileReport.getLength();
				}

				return totalFileSize;
			}

			@Override
			public int getArchiveFileCount() {

				return 0;
			}

			@Override
			public int getErrorCount() {

				return 0;
			}

			@Override
			public FileReport getFileReport(final String path) {

				checkNotNull(path, "path");

				final FileReport fileReport = fileReports.get(path);

				checkArgument(fileReport != null, "Unknown path: " + path);

				return fileReport;
			}

			@Override
			public FolderReport getFolderReport(final String path) {

				checkNotNull(path, "path");

				final FolderReport folderReport = folderReports.get(path);

				checkArgument(folderReport != null, "Unknown path: " + path);

				return folderReport;
			}

			@Override
			public String getHostname() {

				return hostname;
			}

			@Override
			public String getSrcPath() {

				return srcPath;
			}

			@Override
			public CrumbType getType() {

				return type;
			}

			@Override
			public Config getConfig() {

				return new Config() {

					@Override
					public String getLogFile() {

						return logFile;
					}

					@Override
					public int getThreadCount() {

						return threadCount;
					}

					@Override
					public boolean getScanHiddenFiles() {

						return scanHiddenFiles;
					}

					@Override
					public boolean getScanInsideArchiveFiles() {

						return scanInsideArchiveFiles;
					}
				};
			}
		};
	}
}
