package net.avcompris.crumbs;

import static com.google.common.base.Preconditions.checkState;
import static org.apache.commons.lang3.CharEncoding.UTF_8;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.jupiter.api.AfterEach;

import net.avcompris.commons3.yaml.Yaml;
import net.avcompris.commons3.yaml.YamlUtils;
import net.avcompris.crumbs.handler.ConsoleCrumbHandler;
import net.avcompris.crumbs.handler.CrumbHandler;
import net.avcompris.crumbs.report.ScanReport;
import net.avcompris.crumbs.report.ScanReportUtils;

public class CrumbScannerConsoleTest extends AbstractCrumbScannerTest {

	private PrintStream oldSystemOut = null;

	private final ByteArrayOutputStream bos = new ByteArrayOutputStream();

	@Override
	protected CrumbScanner getScanner() throws IOException {

		checkState(oldSystemOut == null, "oldSystemOut should be null");

		oldSystemOut = System.out;

		bos.reset();

		System.setOut(new PrintStream(bos));

		final CrumbHandler handler = new ConsoleCrumbHandler(System.out);

		return new CrumbScanner(System.out, System.err, handler);
	}

	@Override
	protected ScanReport getReport() throws IOException {

		checkState(oldSystemOut != null, "oldSystemOut should not be null");

		System.out.flush();

		System.setOut(oldSystemOut);

		oldSystemOut = null;

		final byte[] bytes = bos.toByteArray();

		System.out.println(new String(bytes, UTF_8));

		final Yaml yaml = YamlUtils.loadYaml(new ByteArrayInputStream(bytes));

		return ScanReportUtils.loadReport(yaml);
	}

	@AfterEach
	public void tearDown() throws Exception {

		if (oldSystemOut != null) {

			System.setOut(oldSystemOut);

			oldSystemOut = null;
		}
	}
}
