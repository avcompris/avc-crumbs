package net.avcompris.crumbs;

import net.avcompris.crumbs.handler.ReportCrumbHandler;
import net.avcompris.crumbs.report.ScanReport;

public class CrumbScannerReportTest extends AbstractCrumbScannerTest {

	private ReportCrumbHandler report;

	@Override
	protected CrumbScanner getScanner() {

		report = new ReportCrumbHandler("xxx", "/xxx/yyy");

		return new CrumbScanner(System.out, System.err, report);
	}

	@Override
	protected ScanReport getReport() {

		return report;
	}
}
