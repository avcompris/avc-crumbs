package net.avcompris.crumbs;

import java.io.File;

import org.junit.jupiter.api.Test;

public class FileListDifferTest {

	@Test
	public void testFileListDiffer_APPC_AT8_Music() throws Exception {

		final FileListDiffer differ = new FileListDiffer();

		differ.listDiffs( //
				FileList.load(new File("src/test/fileLists", "APPC_Music-2022-08-20-09h24-files.txt"),
						"/Volumes/Nouveau nom/Root/Music"), //
				FileList.load(new File("src/test/fileLists", "AT8_Music-2022-08-20-09h21-files.txt"),
						"/Volumes/AT8/Music"));
	}

	@Test
	public void testFileListDiffer_APPC_AT8_Movies() throws Exception {

		final FileListDiffer differ = new FileListDiffer();

		differ.listDiffs( //
				FileList.load(new File("src/test/fileLists", "APPC_Movies-2022-08-20-10h20-files.txt"),
						"/Volumes/Nouveau nom/Root/Movies"), //
				FileList.load(new File("src/test/fileLists", "AT8_Movies-2022-08-20-17h04-files.txt"),
						"/Volumes/AT8/Movies"));
	}

	@Test
	public void testFileListDiffer_APPC_AT8_Archives() throws Exception {

		final FileListDiffer differ = new FileListDiffer();

		differ.listDiffs( //
				FileList.load(new File("src/test/fileLists", "APPC_Archives-2022-08-20-09h24-files.txt"),
						"/Volumes/Nouveau nom/Root/Archives"), //
				FileList.load(new File("src/test/fileLists", "AT8_Archives-2022-08-20-17h03-files.txt"),
						"/Volumes/AT8/Archives"));
	}

	@Test
	public void testFileListDiffer_T5_AT8_T5_SPSSD() throws Exception {

		final FileListDiffer differ = new FileListDiffer();

		differ.listDiffs( //
				FileList.load(new File("src/test/fileLists", "AT8_T5_files.txt"), //
						"/media/AT8/T5-SPSSD"), //
				FileList.load(new File("src/test/fileLists", "T5_files.txt"), //
						"/Volumes/T5-SPSSD"));
	}
}
