package net.avcompris.crumbs;

import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.crumbs.handler.FileReport;
import net.avcompris.crumbs.handler.FolderReport;
import net.avcompris.crumbs.report.ScanReport;

public abstract class AbstractCrumbScannerTest {

	private CrumbScanner scanner;

	private static final ScanConfig config = instantiate(ScanConfig.class) //
			.setLogFile("/tmp/avc-crumbs.log") //
			.setDryRun(false) //
			.setThreadCount(1) //
			.setScanHiddenFiles(false) //
			.setScanInsideArchiveFiles(false);

	protected abstract CrumbScanner getScanner() throws IOException;

	protected abstract ScanReport getReport() throws IOException;

	@BeforeEach
	public final void setUpScanner() throws IOException {

		scanner = getScanner();
	}

	@Test
	public final void testScan999Inexistent() throws Exception {

		assertThrows(FileNotFoundException.class, () ->

		scanner.scan(new File("src/test", "sample-999-inexistent"), config));
	}

	@Test
	public final void testScan000Empty() throws Exception {

		scanner.scan(new File("src/test", "sample-000-empty"), config);

		final ScanReport report = getReport();

		assertEquals(1, report.getFolderCount());
		assertEquals(0, report.getFileCount());
		assertEquals(0L, report.getTotalFileSize());
		assertEquals(0, report.getArchiveFileCount());
		assertEquals(0, report.getErrorCount());

		final FolderReport folder = report.getFolderReport("");

		assertEquals("", folder.getPath());
	}

	@Test
	public final void testScan001OneFileFolder() throws Exception {

		scanner.scan(new File("src/test", "sample-001-oneFile"), config);

		final ScanReport report = getReport();

		assertEquals(1, report.getFolderCount());
		assertEquals(1, report.getFileCount());
		assertEquals(0L, report.getTotalFileSize());
		assertEquals(0, report.getArchiveFileCount());
		assertEquals(0, report.getErrorCount());

		final FolderReport folder = report.getFolderReport("");

		assertEquals("", folder.getPath());

		final FileReport file = report.getFileReport("toto");

		assertEquals("toto", file.getPath());
		assertEquals(0L, file.getLength());
		assertEquals("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", file.getSha256());
	}

	@Test
	public final void testScan001OneFileItself() throws Exception {

		scanner.scan(new File("src/test", "sample-001-oneFile/toto"), config);

		final ScanReport report = getReport();

		assertEquals(0, report.getFolderCount());
		assertEquals(1, report.getFileCount());
		assertEquals(0L, report.getTotalFileSize());
		assertEquals(0, report.getArchiveFileCount());
		assertEquals(0, report.getErrorCount());

		final FileReport file = report.getFileReport("toto");

		assertEquals("toto", file.getPath());
		assertEquals(0L, file.getLength());
		assertEquals("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855", file.getSha256());
	}

	@Test
	public final void testScan002SubFolder() throws Exception {

		scanner.scan(new File("src/test", "sample-002-subFolder"), config);

		final ScanReport report = getReport();

		assertEquals(2, report.getFolderCount());
		assertEquals(1, report.getFileCount());
		assertEquals(0L, report.getTotalFileSize());
		assertEquals(0, report.getArchiveFileCount());
		assertEquals(0, report.getErrorCount());

		assertEquals("", report.getFolderReport("").getPath());
		assertEquals("empty", report.getFolderReport("empty").getPath());

		assertEquals("toto", report.getFileReport("toto").getPath());
	}

	@Test
	public final void testScan003SubFolderWithFiles() throws Exception {

		scanner.scan(new File("src/test", "sample-003-subFolderWithFiles"), config);

		final ScanReport report = getReport();

		assertEquals(2, report.getFolderCount());
		assertEquals(2, report.getFileCount());
		assertEquals(14L, report.getTotalFileSize());
		assertEquals(0, report.getArchiveFileCount());
		assertEquals(0, report.getErrorCount());

		assertEquals("", report.getFolderReport("").getPath());
		assertEquals("twoFiles", report.getFolderReport("twoFiles").getPath());

		final FileReport file0 = report.getFileReport("twoFiles/1");

		assertEquals("twoFiles/1", file0.getPath());
		assertEquals(7L, file0.getLength());
		assertEquals("8238cec92f325b94d3303f87998b85c1c7c3e6d4216e5db7a6ffb1e36a862bab", file0.getSha256());

		final FileReport file1 = report.getFileReport("twoFiles/2");

		assertEquals("twoFiles/2", file1.getPath());
		assertEquals(7L, file1.getLength());
		assertEquals("e42d71aa5c9022a82b900bb4a319bf76a5f137d20b21f1723aca57ab19d486c2", file1.getSha256());
	}
}
