package net.avcompris.crumbs;

import java.io.File;

import org.junit.jupiter.api.Test;

public class FileListCrumbsDifferTest {

	@Test
	public void testFileListCrumbsDiffer_APPC_AT8_Music() throws Exception {

		final FileListCrumbsDiffer differ = new FileListCrumbsDiffer();

		differ.listDiffs( //
				FileListCrumbs.load(new File("src/test/fileLists", "APPC_Music.crumbs.txt"),
						"/Volumes/Nouveau nom/Root/Music"), //
				FileListCrumbs.load(new File("src/test/fileLists", "AT8_Music.crumbs.txt"), "/Volumes/AT8/Music"));
	}

	@Test
	public void testFileListCrumbsDiffer_APPC_AT8_Movies() throws Exception {

		final FileListCrumbsDiffer differ = new FileListCrumbsDiffer();

		differ.listDiffs( //
				FileListCrumbs.load(new File("src/test/fileLists", "APPC_Movies.crumbs.txt"),
						"/Volumes/Nouveau nom/Root/Movies"), //
				FileListCrumbs.load(new File("src/test/fileLists", "AT8_Movies.crumbs.txt"), "/Volumes/AT8/Movies"));
	}

	@Test
	public void testFileListCrumbsDiffer_APPC_AT8_Archives() throws Exception {

		final FileListCrumbsDiffer differ = new FileListCrumbsDiffer();

		differ.listDiffs( //
				FileListCrumbs.load(new File("src/test/fileLists", "APPC_Archives.crumbs.txt"),
						"/Volumes/Nouveau nom/Root/Archives"), //
				FileListCrumbs.load(new File("src/test/fileLists", "AT8_Archives.crumbs.txt"),
						"/Volumes/AT8/Archives"));
	}
}
