package net.avcompris.crumbs.report;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

import net.avcompris.crumbs.handler.CrumbType;
import net.avcompris.crumbs.handler.FileReport;
import net.avcompris.crumbs.handler.FolderReport;

public class ScanReportTest {

	@Test
	public void testCrumbsLoader() throws Exception {

		final ScanReport scan = ScanReportUtils.loadReport(new File("src/test/yaml", "crumbs_soy8_var_movies"));

		assertEquals("xxx", scan.getHostname());
		assertEquals("/var/movies", scan.getSrcPath());
		assertEquals(CrumbType.folder, scan.getType());
		assertEquals("/tmp/avc-crumbs.log", scan.getConfig().getLogFile());
		assertEquals(1, scan.getConfig().getThreadCount());
		assertEquals(false, scan.getConfig().getScanHiddenFiles());
		assertEquals(false, scan.getConfig().getScanInsideArchiveFiles());

		assertEquals(0, scan.getErrorCount());
		assertEquals(32, scan.getFolderCount());
		assertEquals(227, scan.getFileCount());
		assertEquals(0, scan.getArchiveFileCount());
		assertEquals(167_878_222_762L, scan.getTotalFileSize());

		final FileReport lelouch = scan.getFileReport("Movies/L'aventure c'est l'aventure, 1972, Claude Lelouch.mkv");

		assertEquals("Movies/L'aventure c'est l'aventure, 1972, Claude Lelouch.mkv", lelouch.getPath());
		assertEquals(3_652_316_700L, lelouch.getLength());
		assertEquals("5a7cf57fac845a7ecee674659b0a3adb2de20d0b3c629ab2e715ab34ac3fab19", lelouch.getSha256());

		final FolderReport masters = scan
				.getFolderReport("TV Series/Masters Of Sex  S01 Season 1 Complete 480p x264 AAC E-Subs [GWC]");

		assertEquals("TV Series/Masters Of Sex  S01 Season 1 Complete 480p x264 AAC E-Subs [GWC]", masters.getPath());
	}
}
