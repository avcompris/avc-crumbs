package net.avcompris.crumbs;

import java.io.File;

import org.junit.jupiter.api.Test;

public class FileListCrumbsTest {

	@Test
	public void testFileListCrumbs_APPC_Music() throws Exception {

		FileListCrumbs.addFilesInError(
				FileList.load(new File("src/test/fileLists", "APPC_Music-2022-08-20-09h24-files-in-error.txt"),
						"/Volumes/Nouveau nom/Root/Music"));

		FileListCrumbs.produceCrumbs(
				FileList.load(new File("src/test/fileLists", "APPC_Music-2022-08-20-09h24-files.txt"),
						"/Volumes/Nouveau nom/Root/Music"), //
				new File("target", "APPC_Music.crumbs.txt"));
	}

	@Test
	public void testFileListCrumbs_APPC_Movies() throws Exception {

		// Device not configured???:
		//
		// Les Compères (1983) BDRip 1080p [HEVC].mkv
		// length: 7874273777 (APPC)
		//         3118465024 (… AT8!)
		// sha256: impossible to tell on APPC

		FileListCrumbs.addFilesInError(
				FileList.load(new File("src/test/fileLists", "APPC_Movies-2022-08-20-10h20-files-in-error.txt"),
						"/Volumes/Nouveau nom/Root/Movies"));

		FileListCrumbs.produceCrumbs(
				FileList.load(new File("src/test/fileLists", "APPC_Movies-2022-08-20-10h20-files.txt"),
						"/Volumes/Nouveau nom/Root/Movies"), //
				new File("target", "APPC_Movies.crumbs.txt"));
	}

	@Test
	public void testFileListCrumbs_APPC_Archives() throws Exception {

	//	FileListCrumbs.addFilesInError(
	//			FileList.load(new File("src/test/fileLists", "APPC_Archives-2022-08-20-09h24-files-in-error.txt"),
	//					"/Volumes/Nouveau nom/Root/Archives"));

		FileListCrumbs.produceCrumbs(
				FileList.load(new File("src/test/fileLists", "APPC_Archives-2022-08-20-09h24-files.txt"),
						"/Volumes/Nouveau nom/Root/Archives"), //
				new File("target", "APPC_Archives.crumbs.txt"));
	}

	@Test
	public void testFileListCrumbs_AT8_Music() throws Exception {

		FileListCrumbs.produceCrumbs(
				FileList.load(new File("src/test/fileLists", "AT8_Music-2022-08-20-09h21-files.txt"),
						"/Volumes/AT8/Music"), //
				new File("target", "AT8_Music.crumbs.txt"));
	}

	@Test
	public void testFileListCrumbs_AT8_Movies() throws Exception {

		FileListCrumbs.produceCrumbs(
				FileList.load(new File("src/test/fileLists", "AT8_Movies-2022-08-20-17h04-files.txt"),
						"/Volumes/AT8/Movies"), //
				new File("target", "AT8_Movies.crumbs.txt"));
	}

	@Test
	public void testFileListCrumbs_AT8_Archives() throws Exception {

		FileListCrumbs.produceCrumbs(
				FileList.load(new File("src/test/fileLists", "AT8_Archives-2022-08-20-17h03-files.txt"),
						"/Volumes/AT8/Archives"), //
				new File("target", "AT8_Archives.crumbs.txt"));
	}
}
