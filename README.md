# avc-crumbs

A tool to scan miscellaneous folders and files
for info – aka crumbs – on storage devices
and index their locations in a repository.

## Usage

Command line:

    $ ./scan_crumbs.sh [options] <path-to-folder>
    
Options:

  * `-h` print the help message
  * `-l <path-to-logFile>` — default: /tmp/avc-crumbs.log
  * `-m <number-of-threads>` — default: 1
  * `-a` also scan hidden files? — default: false
  * `-z` scan inside zip, gz, tar and tgz files? — default: false

## Example

    $ ./scan_crumbs.sh -m 2 /var/archives
    
## Output

The output is a YAML stream of the following form:

    version: 0.0.1-SNAPSHOT
    
    scan:
      startedAt: 2020-12-03 10:24:00 CEST
      hostname: soy8.local
      srcPath: /var/archives
      type: folder
      config:
        logFile: /tmp/avc-crumbs.log
        threadCount: 1
        scanHiddenFiles: false
        scanInsideArchiveFiles: false
        knownArchiveFileExtensionTypes:
          zip: zip
          tar: tar
          [tgz, tar.gz]: tgz

    crumbs:
      - type: folder
        path: a/b/c
      - type: file
        path: a/b/c/my_super_movie.mp4
        length: 120341
        lastModifiedAt: 2020-11-30 04:45:23 CEST
        sha256: 1b602ecf66...
      - type: folder
        path: d/e/f
      - type: file
        path: a/b/c/my_super_movie_FINAL.mp4
        length: 149122
        lastModifiedAt: 2020-11-30 09:02:11 CEST
        sha256: 73affb07e01...
      - type: file
        archiveType: zip
        path: a/b/c/extra_file.zip
        length: 9123015012
        lastModifiedAt: 2020-11-30 09:02:11 CEST
        error:
          errorAt: 2020-12-03 10:24:39 CEST
          message: |
            I/O read error.
          stackTrace: |
            Xxx

    status:
      endedAt: 2020-12-03 10:25:01 CEST
      elapsedMs: 61123
      folderCount: 2
      fileCount: 3
      totalFileSize: 9123015012
      archiveFileCount: 1
      errorCount: 1

Please note:

* All files info come with a SHA-256 hash of the file content.
* Crumbs – that is, folders and files info – are dumped
as a list, not as a tree. Though files info are dumped after info about their containing folders,
they are not dumped as subtrees.
* Also, folders info may appear at any place – only
after their own containing folder –, even when
another folder is currently being scanned.
 
## Multithreading

Multithreading can be used to calculate SHA hashes
in parallel.

Example:

    ---------------------------------------------------
    threadCount   fileCount   totalFileSize   elapsedMs
    ---------------------------------------------------
              1        1607    448237611118     2834749
    ---------------------------------------------------

## How to build

Run:

    $ mvn install assembly:single

## How to compare source/target files after a copy

### To make sure all files have been copied

Run on the source _and_ on the target:

    $ find /path/to/dir -type f > /tmp/my_list_source.txt
    
Then use `FileListDifferTest`
to compare the resulting two files.

### To make sure all files are the same

Run `FileListCrumbsTest` to produce `xxx_crumbs.txt`
files with lengths and SHA-256s,
then run `FileListCrumbsDifferTest`
to compare them.
