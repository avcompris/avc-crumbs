#!/bin/sh

# Use this script in development mode, to run tests.
#

SRC=src/test/sample-000-empty

# SRC=~/Downloads/torrents

M2_REPO=~/.m2/repository

CLASSPATH=target/classes
CLASSPATH=${CLASSPATH}:${M2_REPO}/com/beust/jcommander/1.72/jcommander-1.72.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/com/google/code/findbugs/jsr305/3.0.0/jsr305-3.0.0.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/com/google/code/findbugs/jsr305/3.0.0/jsr305-3.0.0.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/com/google/guava/guava/23.0/guava-23.0.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/org/apache/commons/commons-lang3/3.4/commons-lang3-3.4.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/org/apache/commons/commons-collections4/4.0/commons-collections4-4.0.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/joda-time/joda-time/2.9.9/joda-time-2.9.9.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/commons-io/commons-io/2.4/commons-io-2.4.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/commons-codec/commons-codec/1.11/commons-codec-1.11.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/net/avcompris/commons/avc-commons3-databeans/0.0.5/avc-commons3-databeans-0.0.5.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/net/avcompris/commons/avc-commons3-types/0.0.5/avc-commons3-types-0.0.5.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/net/avcompris/commons/avc-commons3-yaml/0.0.5/avc-commons3-yaml-0.0.5.jar
CLASSPATH=${CLASSPATH}:${M2_REPO}/org/yaml/snakeyaml/1.24/snakeyaml-1.24.jar

java -cp "${CLASSPATH}" net.avcompris.crumbs.command.Main "${SRC}"